using System;
using System.ComponentModel.DataAnnotations;
using Core.Base;
using Microsoft.AspNetCore.Identity;

namespace Domain.Aggregates.IdentityAggregate;

public sealed class ApplicationUser : IdentityUser<Guid>, IAggregate
{
    [Display(Name = "Full Name")]
    public string FullName { get; set; }    
}