using System;
using Microsoft.AspNetCore.Identity;

namespace Domain.Aggregates.IdentityAggregate;

public sealed class ApplicationRole : IdentityRole<Guid>
{
}