using Core.Base;

namespace Domain.Aggregates.IdentityAggregate;

public interface IUserRepository : IBaseRepository<ApplicationUser>
{
}