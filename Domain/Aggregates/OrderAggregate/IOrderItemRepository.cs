using Core.Base;

namespace Domain.Aggregates.OrderAggregate;

public interface IOrderItemRepository : IBaseRepository<OrderItem>
{
}