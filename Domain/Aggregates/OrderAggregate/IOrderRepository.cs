using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Base;

namespace Domain.Aggregates.OrderAggregate;

public interface IOrderRepository : IBaseRepository<Order>
{
    #region Methods

    /// <summary>
    /// Retrieve a collection of orders by user id
    /// </summary>
    /// <returns>An asynchronous task that returns a collection of orders</returns>
    Task<List<Order>> GetOrdersAsync();

    #endregion
}