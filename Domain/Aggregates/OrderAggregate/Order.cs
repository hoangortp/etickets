﻿using System;
using System.Collections.Generic;
using Core.Base;
using Domain.Aggregates.IdentityAggregate;
using Domain.Entities;

namespace Domain.Aggregates.OrderAggregate;

public class Order : BaseEntity, IAggregate
{
	#region Properties

	public string Email { get; set; }

	public Guid UserId { get; set; }

	#endregion

	#region Relationship

	public List<OrderItem> OrderItems { get; set; }

	public ApplicationUser User { get; set; }

	#endregion
}