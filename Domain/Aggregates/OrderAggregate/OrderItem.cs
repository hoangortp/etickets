﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities;

namespace Domain.Aggregates.OrderAggregate;

public class OrderItem : BaseEntity
{
	#region Properties

	public int Amount { get; set; }

	public double Price { get; set; }

	#endregion

	#region Relationship

	#region Movie

	public Guid MovieId { get; set; }
	[ForeignKey("MovieId")]

	public Movie Movie { get; set; }

	#endregion

	#region Order

	public Guid OrderId { get; set; }
	[ForeignKey("OrderId")]

	public Order Order { get; set; }

	#endregion

	#endregion
}