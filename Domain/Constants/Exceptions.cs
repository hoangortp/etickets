namespace Domain.Constants;

public static class Exceptions
{
    public const string ActorNotFound = "Actor not found!";
    public const string CinemaNotFound = "Cinema not found!";
    public const string MovieNotFound = "Movie not found!";
    public const string ProducerNotFound = "Producer not found!";
}