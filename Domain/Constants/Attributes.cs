namespace Domain.Constants;

public static class Attributes
{
    #region Display(Name)

    // General
    public const string ProfilePicture = "Public Profile";
    public const string FullName = "Full Name";
    public const string Biography = "Biography";
    
    // Movie Request
    public const string MovieName = "Movie name";
    public const string MovieDescription = "Movie description";
    public const string MoviePrice = "Movie price";
    public const string MoviePosterUrl = "Movie poster url";
    public const string StartDate = "Start date";
    public const string EndDate = "End date";
    public const string CategorySelect = "Select a category";
    public const string ActorsSelect = "Select actor(s)";
    public const string CinemaSelect = "Select a cinema";
    public const string ProducerSelect = "Select a producer";
    
    // Login and Register
    public const string ConfirmPassword = "Confirm Password";
    
    #endregion

    #region ErrorMessage: Required

    public const string ProfilePictureRequired = "Profile Picture is required!";
    public const string FullNameRequired = "Full Name is required!";
    public const string BiographyRequired = "Biography is required!";
    public const string LogoRequired = "Logo is required!";
    public const string NameRequired = "Name is required!";
    public const string DescriptionRequired = "Description is required!";
    
    // Movie Request
    public const string PriceRequired = "Price is required!";
    public const string MoviePosterUrlRequired = "Movie Poster Url is required";
    public const string StartDateRequired = "Start Date is required";
    public const string EndDateRequired = "End Date is required";
    public const string CategoryRequired = "Category is required";
    public const string ActorsRequired = "Actor is required";
    public const string CinemaRequired = "Cinema is required";
    public const string ProducerRequired = "Producer is required";
    
    // Login and Register View Model
    public const string EmailRequired = "Email is required!";
    public const string PasswordRequired = "Password is required!";
    public const string ConfirmPasswordRequired = "Confirm password is required!";
    
    #endregion

    #region ErrorMessage: Other

    public const string FullNameLimitation = "Full Name must be between 3 and 50 characters";
    public const string NameLimitation = "Name must be between 3 and 50 characters";
    public const string PasswordNotMatch = "Password do not match!";

    #endregion
}