﻿using Core.Base;
using Domain.Entities;

namespace Domain.Repositories;

public interface ICinemaRepository : IBaseRepository<Cinema>
{
}