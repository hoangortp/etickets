﻿using Core.Base;
using Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Domain.Repositories;

public interface IActorMovieRepository : IBaseRepository<ActorMovie>
{
	#region Methods

	/// <summary>
	/// Delete a collection of ActorMovie by movie id
	/// </summary>
	/// <param name="movieId">Movie id to retrieve an expected collection of ActorMovie</param>
	/// <returns>An asynchronous task</returns>
	Task DeleteActorsMoviesByMovieIdAsync(Guid movieId);

	#endregion
}