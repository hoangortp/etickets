using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Base;
using Domain.Entities;

namespace Domain.Repositories;

public interface IShoppingCartItemRepository : IBaseRepository<ShoppingCartItem>
{
    #region Methods

    /// <summary>
    /// Retrieve shopping cart item by movie id and shopping cart id
    /// </summary>
    /// <param name="movieId">Movie id to retrieve expected item</param>
    /// <param name="shoppingCartId">Shopping cart id to retrieve expected item</param>
    /// <returns>An asynchronous task that returns a shopping cart item</returns>
    Task<ShoppingCartItem> GetByMovieIdAndShoppingCartIdAsync(Guid movieId, string shoppingCartId);

    /// <summary>
    /// Retrieve a collection of shopping cart items including movie by shopping cart id
    /// </summary>
    /// <param name="shoppingCartId">Shopping cart id to retrieve expected a collection of items</param>
    /// <returns>An asynchronous task that returns a collection of items</returns>
    Task<List<ShoppingCartItem>> GetAllByShoppingCartIdIncludeMovieAsync(string shoppingCartId);

    /// <summary>
    /// Retrieve subtotal price of shopping cart items
    /// </summary>
    /// <param name="shoppingCartId">Shopping cart id to retrieve expected collection of items</param>
    /// <returns>An asynchronous task that returns sum of items price with amounts as subtotal</returns>
    Task<double> GetTotalAsync(string shoppingCartId);

    /// <summary>
    /// Clear all the items contained in shopping cart
    /// </summary>
    /// <param name="shoppingCartId">Shopping cart id to retrieve expected collection of items</param>
    /// <returns>An asynchronous task</returns>
    Task ClearShoppingCartAsync(string shoppingCartId);

    #endregion
}