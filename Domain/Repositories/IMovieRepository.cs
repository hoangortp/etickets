﻿using Core.Base;
using Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Domain.Repositories;

public interface IMovieRepository : IBaseRepository<Movie>
{
	#region Methods
	
	/// <summary>
	/// Retrieve asynchronously a movie by id including cinema, producer, and actor
	/// </summary>
	/// <param name="id">Id to retrieve an expected movie</param>
	/// <returns>An asynchronous task that returns a movie</returns>
	Task<Movie> GetAndIncludeByIdAsync(Guid id);

	#endregion
}