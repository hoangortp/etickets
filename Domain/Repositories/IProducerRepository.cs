﻿using Core.Base;
using Domain.Entities;

namespace Domain.Repositories;

public interface IProducerRepository : IBaseRepository<Producer>
{
}