﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities;

public class Movie : BaseEntity
{
	#region Properties

	public string Name { get; set; }

	public string Description { get; set; }

	public double Price { get; set; }

	public string ImageUrl { get; set; }

	public DateTime StartDate { get; set; }

	public DateTime EndDate { get; set; }

	public MovieCategory MovieCategory { get; set; }

	#endregion

	#region Relationships

	#region ActorMovie

	/// <summary>
	/// One movie can have many actors
	/// </summary>
	public ICollection<ActorMovie> ActorsMovies { get; set; }

	#endregion

	#region Cinema

	/// <summary>
	/// One movie can be purchased from only one cinema
	/// </summary>
	public Guid CinemaId { get; set; }
	[ForeignKey("CinemaId")]

	public Cinema Cinema { get; set; }

	#endregion

	#region Producer

	/// <summary>
	/// One movie can be produced by only one producer
	/// </summary>
	public Guid ProducerId { get; set; }
	[ForeignKey("ProducerId")]

	public Producer Producer { get; set; }

	#endregion

	#endregion
}