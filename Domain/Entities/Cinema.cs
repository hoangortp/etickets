﻿using System.Collections.Generic;

namespace Domain.Entities;

public class Cinema : BaseEntity
{
	#region Properties

	public string Logo { get; set; }

	public string Name { get; set; }

	public string Description { get; set; }

	#endregion

	#region Relationship

	/// <summary>
	/// One cinema can show many movies
	/// </summary>
	public ICollection<Movie> Movies { get; set; }

	#endregion
}