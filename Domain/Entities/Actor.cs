﻿using System.Collections.Generic;

namespace Domain.Entities;

public class Actor : BaseEntity
{
	#region Properties

	public string ProfilePictureUrl { get; set; }

	public string FullName { get; set; }

	public string Bio { get; set; }

	#endregion

	#region Relationships

	/// <summary>
	/// One actor can play in many movies
	/// </summary>
	public ICollection<ActorMovie> ActorsMovies { get; set; }

	#endregion
}