using System;

namespace Domain.Entities;

public class ShoppingCartItem : BaseEntity
{
    #region Properties

    public int Amount { get; set; }

    public string ShoppingCartId { get; set; }

    #endregion
    
    #region Relationship

    public Guid MovieId { get; set; }
    
    public Movie Movie { get; set; }

    #endregion
}