﻿using Core.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities;

public class BaseEntity : IBaseEntity
{
	#region Properties

	[Key]
	public Guid Id { get; set; }

	#endregion
}