﻿using System;

namespace Domain.Entities;

public class ActorMovie
{
	#region Relationships
		
	#region Movie
	
	public Guid MovieId { get; set; }
	
	public Movie Movie { get; set; }
	
	#endregion

	#region Actor

	public Guid ActorId { get; set; }
	
	public Actor Actor { get; set; }

	#endregion
	
	#endregion
}