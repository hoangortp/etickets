﻿using System.Collections.Generic;

namespace Domain.Entities;

public class Producer : BaseEntity
{
	#region Properties

	public string ProfilePictureUrl { get; set; }

	public string FullName { get; set; }

	public string Bio { get; set; }

	#endregion

	#region Relationships

	/// <summary>
	/// One producer can produce many movies
	/// </summary>
	public ICollection<Movie> Movies { get; set; }

	#endregion
}