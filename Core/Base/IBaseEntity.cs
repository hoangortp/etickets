﻿namespace Core.Base;

public interface IBaseEntity
{
	#region Properties

	Guid Id { get; set; }

	#endregion
}