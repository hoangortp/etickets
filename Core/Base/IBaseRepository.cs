﻿using System.Linq.Expressions;

namespace Core.Base;

/// <summary>
/// Define methods to manipulate generic repository
/// </summary>
/// <typeparam name="T">Type of the entity in the repository</typeparam>
public interface IBaseRepository<T> where T : class, new()
{
    #region Methods
        
    /// <summary>
    /// Add an entity to the collection of T
    /// </summary>
    /// <param name="entity">An entity to be added</param>
    /// <returns>An asynchronous task</returns>
    Task AddAsync(T entity);

    /// <summary>
    /// Retrieve a collection of T based on filter, included properties and tracking
    /// </summary>
    /// <param name="filter">A filter expression to query data</param>
    /// <param name="orderBy">A func delegate to sort the data in order</param>
    /// <param name="includeProperties">Specify the properties that including in return</param>
    /// <param name="tracked">A flag to specify if DbSet should be tracked by DbContext</param>
    /// <returns>An asynchronous task that returns a collection of T</returns>
    Task<ICollection<T>> GetAllAsync(Expression<Func<T, bool>>? filter = null,
		                             Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
									 Expression<Func<T, object>>[]? includeProperties = null,
                                     bool tracked = false);

    /// <summary>
    /// Retrieve a single instance of T based on filter, including properties and tracking
    /// </summary>
    /// <param name="filter">A filter expression to query data</param>
    /// <param name="includeProperties">Specify the properties that including in return</param>
    /// <param name="tracked">A flag to specify if DbSet should be tracked by DbContext</param>
    /// <returns>An asynchronous task that returns an instance of T</returns>
    Task<T?> GetAsync(Expression<Func<T, bool>> filter,
                      Expression<Func<T, object>>[]? includeProperties = null,
                      bool tracked = false);

    /// <summary>
    /// Remove an entity from the collection of T
    /// </summary>
    /// <param name="entity">An entity to be removed</param>
    /// <returns>A task marked as completed</returns>
    Task RemoveAsync(T entity);

    /// <summary>
    /// Remove a collection of T
    /// </summary>
    /// <param name="entities">A collection of T to be removed</param>
    /// <returns>A task marked as completed</returns>
    Task RemoveRangeAsync(ICollection<T> entities);

    /// <summary>
    /// Update entity details 
    /// </summary>
    /// <param name="entity">An entity to be updated</param>
    /// <returns>A task marked as completed</returns>
    Task UpdateAsync(T entity);
        
    #endregion
}