﻿using Application.Commands.Actor;
using Application.Queries.Actor;
using Application.Responses;
using AutoMapper;
using Domain.Aggregates.IdentityAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Presentation.Controllers;

[Authorize(Roles = Roles.Admin)]
public class ActorController : Controller
{
    #region Fields

    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    #endregion

    #region Constructors

    /// <summary>
    /// Initialize an instance of ActorController
    /// </summary>
    /// <param name="mapper">An abstract of Mapper</param>
	/// <param name="mediator">An anstract of Mediator</param>
    public ActorController(IMapper mapper, IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Get the page that contains the list of actors
    /// </summary>
    /// <returns>A view of actors</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Index()
    {
        var query = new GetActorsQuery();

        var actors = await _mediator.Send(query);

        var response = _mapper.Map<ICollection<ActorResponse>>(actors);

        return View(response);
    }

    /// <summary>
    /// Get the page of creating new actor
    /// </summary>
    /// <returns>A view of creating new actor</returns>
    public IActionResult Create()
    {
        return View();
    }

    /// <summary>
    /// Create a new actor
    /// </summary>
    /// <param name="command">An command to create new actor</param>
    /// <returns>Redirect to the page of actors</returns>
    [HttpPost]
    public async Task<IActionResult> Create(CreateActorCommand command)
    {
        if (!ModelState.IsValid)
        {
            return View(command);
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    /// <summary>
    /// Get the page of actor details based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected actor</param>
    /// <returns>A view of actor details</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Details(Guid id)
    {
        var query = new GetActorByIdQuery(id);

        var reponse = await _mediator.Send(query);

        if (reponse is null)
        {
            return View("NotFound");
        }

        return View(reponse);
    }

    /// <summary>
    /// Get the page of editing actor based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected actor</param>
    /// <returns>A view of actor details</returns>
    public async Task<IActionResult> Edit(Guid id)
    {
        var query = new GetActorByIdQuery(id);

        var response = await _mediator.Send(query);

        if (response is null)
        {
            return View("NotFound");
        }

        var command = EditActorCommand.Create(response);

        return View(command);
    }

    /// <summary>
    /// Edit an actor by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected actor to edit</param>
    /// <param name="command">A command to edit an actor</param>
    /// <returns>Redirect to the page of actors</returns>
    [HttpPost]
    public async Task<IActionResult> Edit(Guid id, EditActorCommand command)
    {
        if (!ModelState.IsValid)
        {
            return View(command);
        }

        command.Id = id;

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    /// <summary>
    /// Get the page of deleting actor based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected actor</param>
    /// <returns>A view of actor details</returns>
    public async Task<IActionResult> Delete(Guid id)
    {
        var query = new GetActorByIdQuery(id);

        var response = await _mediator.Send(query);

        if (response is null)
        {
            return View("NotFound");
        }

        return View(response);
    }

    /// <summary>
    /// Delete an actor by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected actor to delete</param>
    /// <param name="command">A command to delete an actor</param>
    /// <returns>Redirect to the page of actors</returns>
    [HttpPost, ActionName("Delete")]
    public async Task<IActionResult> DeleteConfirm(Guid id, DeleteActorCommand command)
    {
        command.Id = id;

        var query = new GetActorByIdQuery(id);

        var response = await _mediator.Send(query);

        if (response is null)
        {
            return View("NotFound");
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    #endregion
}