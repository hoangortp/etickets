﻿using Application.Commands.Producer;
using Application.Queries.Producer;
using AutoMapper;
using Domain.Aggregates.IdentityAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Presentation.Controllers;

[Authorize(Roles = Roles.Admin)]
public class ProducerController : Controller
{
    #region Fields

    private readonly IMapper _mapper;
    private readonly IMediator _mediator;

    #endregion

    #region Constructors

    /// <summary>
    /// Initialize an instance of ProducerController
    /// </summary>
    /// <param name="mapper">An abstract of Mapper</param>
    /// <param name="mediator">An abstract of Mediator</param>
    public ProducerController(IMapper mapper, IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Get the page that contains the list of producers
    /// </summary>
    /// <returns>A view of producers</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Index()
    {
        var query = new GetProducersQuery();

        var response = await _mediator.Send(query);

        return View(response);
    }

    /// <summary>
    /// Get the page of creating new producer
    /// </summary>
    /// <returns>A view of creating new producer</returns>
    public IActionResult Create()
    {
        return View();
    }

    /// <summary>
    /// Create a new producer
    /// </summary>
    /// <param name="command">A command to create new producer</param>
    /// <returns>Redirect to the page of producers</returns>
    [HttpPost]
    public async Task<IActionResult> Create(CreateProducerCommand command)
    {
        if (!ModelState.IsValid)
        {
            return View(command);
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    /// <summary>
    /// Get the page of deleting producer based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected producer</param>
    /// <returns>A view of producer details</returns>
    public async Task<IActionResult> Delete(Guid id)
    {
        var query = new GetProducerByIdQuery(id);

        var response = await _mediator.Send(query);

        if (query is null)
        {
            return View("NotFound");
        }

        return View(response);
    }

    /// <summary>
    /// Delete a producer by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected producer to delete</param>
    /// <returns>Redirect to the page of producers</returns>
    [HttpPost, ActionName("Delete")]
    public async Task<IActionResult> DeleteConfirm(Guid id, DeleteProducerCommand command)
    {
        command.Id = id;

        var query = new GetProducerByIdQuery(id);

        var response = await _mediator.Send(query);

        if (response is null)
        {
            return View("NotFound");
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    /// <summary>
    /// Get the page of producer details based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected producer</param>
    /// <returns>A view of producer details</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Details(Guid id)
    {
        var query = new GetProducerByIdQuery(id);

        var response = await _mediator.Send(query);

        if (query is null)
        {
            return View("NotFound");
        }

        return View(response);
    }

    /// <summary>
    /// Get the page of editing producer based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected producer</param>
    /// <returns>A view of producer details</returns>
    public async Task<IActionResult> Edit(Guid id)
    {
        var query = new GetProducerByIdQuery(id);

        var response = await _mediator.Send(query);

        if (response is null)
        {
            return View("NotFound");
        }

        var command = EditProducerCommand.Create(response);

        return View(command);
    }

    /// <summary>
    /// Edit a producer by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected producer to edit</param>
    /// <param name="command">A command to edit a producer</param>
    /// <returns>Redirect to the page of producers</returns>
    [HttpPost]
    public async Task<IActionResult> Edit(Guid id, EditProducerCommand command)
    {
        command.Id = id;

        if (!ModelState.IsValid)
        {
            return View(command);
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    #endregion
}