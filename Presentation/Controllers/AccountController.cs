using Application.Queries.Users;
using Domain.Aggregates.IdentityAggregate;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using System.Threading.Tasks;

namespace Presentation.Controllers;

public class AccountController : Controller
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IMediator _mediator;

    public AccountController(UserManager<ApplicationUser> userManager,
                             SignInManager<ApplicationUser> signInManager,
                             IMediator mediator)
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _mediator = mediator;
    }

    public async Task<IActionResult> Users()
    {
        var query = new GetUsersQuery();

        var response = await _mediator.Send(query);

        return View(response);
    }

    public IActionResult Login()
    {
        return View(new LoginViewModel());
    }

    [HttpPost]
    public async Task<IActionResult> Login(LoginViewModel loginViewModel)
    {
        if (!ModelState.IsValid)
        {
            return View(loginViewModel);
        }

        var user = await _userManager.FindByEmailAsync(loginViewModel.Email);

        if (user is not null)
        {
            var passwordCheck = await _userManager.CheckPasswordAsync(user, loginViewModel.Password);

            if (passwordCheck)
            {
                var result = await _signInManager.PasswordSignInAsync(user, loginViewModel.Password, false, false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Movie");
                }
            }

            TempData["Error"] = "Username or password is incorrect! Please try again!";

            return View(loginViewModel);
        }

        TempData["Error"] = "Username or password is incorrect! Please try again!";

        return View(loginViewModel);
    }

    public IActionResult Register()
    {
        return View(new RegisterViewModel());
    }

    [HttpPost]
    public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
    {
        if (!ModelState.IsValid)
        {
            return View(registerViewModel);
        }

        var user = await _userManager.FindByEmailAsync(registerViewModel.Email);

        if (user is not null)
        {
            TempData["Error"] = "This email is already registered!";

            return View(registerViewModel);
        }

        var newUser = new ApplicationUser()
        {
            FullName = registerViewModel.FullName,
            Email = registerViewModel.Email,
            UserName = registerViewModel.Email,
        };

        var newUserResponse = await _userManager.CreateAsync(newUser, registerViewModel.Password);

        if (newUserResponse.Succeeded)
        {
            await _userManager.AddToRoleAsync(newUser, Roles.User);
        }

        return View("RegisterSuccessfully");
    }

    [HttpPost]
    public async Task<IActionResult> Logout()
    {
        await _signInManager.SignOutAsync();

        return RedirectToAction("Index", "Movie");
    }

    public IActionResult AccessDenied(string returnUrl)
    {
        return View();
    }
}