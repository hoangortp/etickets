﻿using Application.Commands.Movie;
using Application.Queries.Movie;
using AutoMapper;
using Domain.Aggregates.IdentityAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation.Controllers;

[Authorize(Roles = Roles.Admin)]
public class MovieController : Controller
{
    #region Fields

    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    #endregion

    #region Constructors

    /// <summary>
    /// Initialize an instance of MovieController
    /// </summary>
    /// <param name="mapper">An abstract of Mapper</param>
    /// <param name="mediator">An abstract of Mediator</param>
    public MovieController(IMapper mapper, IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Get the page that contains the list of movies
    /// </summary>
    /// <returns>A view of movies</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Index()
    {
        var query = new GetMoviesQuery();

        var response = await _mediator.Send(query);

        return View(response);
    }

    /// <summary>
    /// Search movies by name or description
    /// </summary>
    /// <param name="searchString">A string to search movie</param>
    /// <returns>A view of searched movies</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Filter(string searchString)
    {
        var query = new GetMoviesQuery();

        var response = await _mediator.Send(query);

        if (!string.IsNullOrEmpty(searchString))
        {
            var filteredResults = response.Where(n => n.Name.Contains(searchString, StringComparison.CurrentCultureIgnoreCase)
                                                       || n.Description.Contains(searchString, StringComparison.CurrentCultureIgnoreCase))
                                          .ToList();

            return View("Index", filteredResults);
        }

        return View("Index", response);
    }

    /// <summary>
    /// Get the page of movie details by id
    /// </summary>
    /// <param name="id">id to retrieve an expected movie</param>
    /// <returns>A view of movie details</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Details(Guid id)
    {
        var query = new GetMovieIncludeByIdQuery(id);

        var response = await _mediator.Send(query);

        if (response is null)
        {
            return View("NotFound");
        }

        return View(response);
    }

    /// <summary>
    /// Get the page of creating new movie
    /// </summary>
    /// <returns>A view of creating new movie</returns>
    public async Task<IActionResult> Create()
    {
        var query = new GetNewMovieDropdownsValuesQuery();

        var movieDropdownsData = await _mediator.Send(query);

        ViewBag.Cinemas = new SelectList(movieDropdownsData.Cinemas, "Id", "Name");
        ViewBag.Producers = new SelectList(movieDropdownsData.Producers, "Id", "FullName");
        ViewBag.Actors = new SelectList(movieDropdownsData.Actors, "Id", "FullName");

        return View();
    }

    /// <summary>
    /// Create a new movie
    /// </summary>
    /// <param name="command">A command to create new move</param>
    /// <returns>Redirect to the page of movies</returns>
    [HttpPost]
    public async Task<IActionResult> Create(CreateMovieCommand command)
    {
        if (!ModelState.IsValid)
        {
            var query = new GetNewMovieDropdownsValuesQuery();

            var movieDropdownsData = await _mediator.Send(query);

            ViewBag.Cinemas = new SelectList(movieDropdownsData.Cinemas, "Id", "Name");
            ViewBag.Producers = new SelectList(movieDropdownsData.Producers, "Id", "FullName");
            ViewBag.Actors = new SelectList(movieDropdownsData.Actors, "Id", "FullName");

            return View(command);
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    /// <summary>
    /// Get the page of editing movie based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected movie</param>
    /// <returns>A view of editing movie with details</returns>
    public async Task<IActionResult> Edit(Guid id)
    {
        var movieIncludeQuery = new GetMovieIncludeByIdQuery(id);

        var movieDetails = await _mediator.Send(movieIncludeQuery);

        if (movieDetails is null)
        {
            return View("NotFound");
        }

        var command = EditMovieCommand.Create(movieDetails);

        var movieDropdownQuery = new GetNewMovieDropdownsValuesQuery();

        var movieDropdownsData = await _mediator.Send(movieDropdownQuery);

        ViewBag.Cinemas = new SelectList(movieDropdownsData.Cinemas, "Id", "Name");
        ViewBag.Producers = new SelectList(movieDropdownsData.Producers, "Id", "FullName");
        ViewBag.Actors = new SelectList(movieDropdownsData.Actors, "Id", "FullName");

        return View(command);
    }

    /// <summary>
    /// Edit a movie by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected movie</param>
    /// <param name="command">A command to edit a movie</param>
    /// <returns>Redirect to the page of movies</returns>
    [HttpPost]
    public async Task<IActionResult> Edit(Guid id, EditMovieCommand command)
    {
        if (!ModelState.IsValid)
        {
            var query = new GetNewMovieDropdownsValuesQuery();

            var movieDropdownsData = await _mediator.Send(query);

            ViewBag.Cinemas = new SelectList(movieDropdownsData.Cinemas, "Id", "Name");
            ViewBag.Producers = new SelectList(movieDropdownsData.Producers, "Id", "FullName");
            ViewBag.Actors = new SelectList(movieDropdownsData.Actors, "Id", "FullName");

            return View(command);
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    #endregion
}