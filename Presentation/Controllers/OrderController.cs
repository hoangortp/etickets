using Application.Commands.Order;
using Application.Queries.Movie;
using Application.Queries.Orders;
using Application.Services;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Presentation.Controllers;

[Authorize]
public class OrderController : Controller
{
    #region Fields

    private readonly ShoppingCartService _shoppingCartService;
    private readonly IMapper _mapper;
    private readonly IMediator _mediator;

    #endregion

    #region Constructors

    public OrderController(ShoppingCartService shoppingCartService,
                           IMapper mapper,
                           IMediator mediator)
    {
        _shoppingCartService = shoppingCartService;
        _mapper = mapper;
        _mediator = mediator;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Get the page that contains a list of orders
    /// </summary>
    /// <returns>A view of orders</returns>
    public async Task<IActionResult> Index()
    {
        Guid userId = Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
        string role = User.FindFirstValue(ClaimTypes.Role);

        var query = new GetOrdersQuery(userId, role);

        var orders = await _mediator.Send(query);

        return View(orders);
    }

    /// <summary>
    /// Get page the shopping cart
    /// </summary>
    /// <param name="movieName">A movie name to get expected cart item</param>
    /// <param name="moviePrice">A movie price to get cart item price</param>
    /// <returns>A view of shopping cart</returns>
    public async Task<IActionResult> ShoppingCart(string movieName, double moviePrice)
    {
        var items = await _shoppingCartService.GetShoppingCartItemsAsync();
        var total = await _shoppingCartService.GetShoppingCartTotalAsync();

        _shoppingCartService.ShoppingCartItems = items;

        ShoppingCartViewModel response = new()
        {
            MovieName = movieName,
            MoviePrice = moviePrice,
            ShoppingCartService = _shoppingCartService,
            ShoppingCartTotal = total
        };

        return View(response);
    }

    /// <summary>
    /// Add movie item into shopping cart
    /// </summary>
    /// <param name="id">Id to retrieve an expected movie item</param>
    /// <returns>Redirect to the page of shopping cart</returns>
    public async Task<RedirectToActionResult> AddItemToShoppingCart(Guid id)
    {
        var query = new GetMovieByIdQuery(id);

        var response = await _mediator.Send(query);

        await _shoppingCartService.AddItemToCartAsync(id);

        return RedirectToAction(nameof(ShoppingCart), new { movieName = response.Name, moviePrice = response.Price });
    }

    /// <summary>
    /// Remove movie item from the shopping cart
    /// </summary>
    /// <param name="id">Id to retrieve an expexted movie item</param>
    /// <returns>Redirect to the page of shopping cart</returns>
    public async Task<RedirectToActionResult> RemoveItemFromShoppingCart(Guid id)
    {
        var query = new GetMovieByIdQuery(id);

        var response = await _mediator.Send(query);

        await _shoppingCartService.RemoveItemFromCartAsync(id);

        return RedirectToAction(nameof(ShoppingCart), new { movieName = response.Name, moviePrice = response.Price });
    }

    /// <summary>
    /// Make new order
    /// </summary>
    /// <returns>A view that announces the order is completed</returns>
    /// <exception cref="Exception">An exception that is thrown if there is no user identifier</exception>
    public async Task<IActionResult> MakeOrder()
    {
        var items = await _shoppingCartService.GetShoppingCartItemsAsync();

        Guid userId = Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier) ?? throw new Exception("Invalid or missing user identifier"));
        string email = User.FindFirstValue(ClaimTypes.Email);

        var command = new StoreOrderCommand(items, userId, email);

        await _mediator.Send(command);
        await _shoppingCartService.ClearShoppingCartAsync();

        return View("OrderCompleted");
    }

    #endregion
}