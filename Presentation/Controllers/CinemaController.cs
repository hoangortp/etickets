﻿using Application.Commands.Cinema;
using Application.Queries.Cinema;
using AutoMapper;
using Domain.Aggregates.IdentityAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Presentation.Controllers;

[Authorize(Roles = Roles.Admin)]
public class CinemaController : Controller
{
    #region Fields

    private readonly IMapper _mapper;
    private readonly IMediator _mediator;

    #endregion

    #region Constructors

    /// <summary>
    /// Initialize an instance of CinemaController
    /// </summary>
    /// <param name="mapper">An abstract of Mapper</param>
    /// <param name="mediator">An abtract of Mediator</param>
    public CinemaController(IMapper mapper, IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Get the page that contains the list of cinemas
    /// </summary>
    /// <returns>A view of cinemas</returns>
    [AllowAnonymous]
    public async Task<IActionResult> Index()
    {
        var query = new GetCinemasQuery();

        var allCinemas = await _mediator.Send(query);

        return View(allCinemas);
    }

    /// <summary>
    /// Get the page of creating new cinema
    /// </summary>
    /// <returns>A view of creating new cinema</returns>
    public IActionResult Create()
    {
        return View();
    }

    /// <summary>
    /// Create a new cinema
    /// </summary>
    /// <param name="command">A command to create new cinema</param>
    /// <returns>Redirect to the page of cinemas</returns>
    [HttpPost]
    public async Task<IActionResult> Create(CreateCinemaCommand command)
    {
        if (!ModelState.IsValid)
        {
            return View(command);
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    /// <summary>
    /// Get the page of deleting cinema based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected cinema</param>
    /// <returns></returns>
    [AllowAnonymous]
    public async Task<IActionResult> Delete(Guid id)
    {
        var query = new GetCinemaByIdQuery(id);

        var cinema = await _mediator.Send(query);

        if (cinema is null)
        {
            return View("NotFound");
        }

        return View(cinema);
    }

    /// <summary>
    /// Delete a cinema by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected cinema to delete</param>
    /// <returns>Redirect to the page of cinemas</returns>
    [HttpPost, ActionName("Delete")]
    public async Task<IActionResult> DeleteConfirm(Guid id, DeleteCinemaCommand command)
    {
        command.Id = id;

        var query = new GetCinemaByIdQuery(id);

        var cinema = await _mediator.Send(query);

        if (cinema is null)
        {
            return View("NotFound");
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    /// <summary>
    /// Get the page of cinema details by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected cinema</param>
    /// <returns>A view of cinema details</returns>
    public async Task<IActionResult> Details(Guid id)
    {
        var query = new GetCinemaByIdQuery(id);

        var cinema = await _mediator.Send(query);

        if (cinema is null)
        {
            return View("NotFound");
        }

        return View(cinema);
    }

    /// <summary>
    /// Get the page of editing cinema based on id
    /// </summary>
    /// <param name="id">Id to retrieve an expected cinema</param>
    /// <returns>A view of editing cinema with details</returns>
    public async Task<IActionResult> Edit(Guid id)
    {
        var query = new GetCinemaByIdQuery(id);

        var cinema = await _mediator.Send(query);

        if (cinema is null)
        {
            return View("NotFound");
        }

        var command = EditCinemaCommand.Create(cinema);

        return View(command);
    }

    /// <summary>
    /// Edit a cinema by id
    /// </summary>
    /// <param name="id">Id to retrieve an expected cinema to edit</param>
    /// <param name="command">A command to edit a cinema</param>
    /// <returns>Redirect to the page of cinemas</returns>
    [HttpPost]
    public async Task<IActionResult> Edit(Guid id, EditCinemaCommand command)
    {
        command.Id = id;

        var query = new GetCinemaByIdQuery(id);

        var cinema = await _mediator.Send(query);

        if (cinema is null)
        {
            return View("NotFound");
        }

        await _mediator.Send(command);

        return RedirectToAction(nameof(Index));
    }

    #endregion
}