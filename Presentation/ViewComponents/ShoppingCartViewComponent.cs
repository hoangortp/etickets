using System.Threading.Tasks;
using Application.Services;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.ViewComponents;

public class ShoppingCartSummary : ViewComponent
{
    private readonly ShoppingCartService _shoppingCartService;

    public ShoppingCartSummary(ShoppingCartService shoppingCartService)
    {
        _shoppingCartService = shoppingCartService;
    }

    public IViewComponentResult Invoke()
    {
        var items = _shoppingCartService.GetShoppingCartItemsAsync().GetAwaiter().GetResult();

        return View(items.Count);
    }
}