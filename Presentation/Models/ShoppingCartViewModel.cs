using Application.Services;
using Infrastructure;

namespace Presentation.Models;

public class ShoppingCartViewModel
{
    #region Properties

    public string MovieName { get; init; }

    public double MoviePrice { get; init; }
    
    public ShoppingCartService ShoppingCartService { get; init; }

    public double ShoppingCartTotal { get; init; }

    #endregion
}