using System.ComponentModel.DataAnnotations;
using Domain.Constants;
using Microsoft.VisualBasic.CompilerServices;

namespace Presentation.Models;

public class RegisterViewModel
{
    [Display(Name = Attributes.FullName)]
    [Required(ErrorMessage = Attributes.FullNameRequired)]
    public string FullName { get; init; }
    
    [Required(ErrorMessage = Attributes.EmailRequired)]
    public string Email { get; init; }
    
    [Required(ErrorMessage = Attributes.PasswordRequired)]
    [DataType(DataType.Password)]
    public string Password { get; init; }
    
    [Display(Name = Attributes.ConfirmPassword)]
    [Required(ErrorMessage = Attributes.ConfirmPasswordRequired)]
    [DataType(DataType.Password)]
    [Compare(nameof(Password), ErrorMessage = Attributes.PasswordNotMatch)]
    public string ConfirmPassword { get; init; }
}