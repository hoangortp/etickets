using System.ComponentModel.DataAnnotations;
using Domain.Constants;
using Microsoft.VisualBasic.CompilerServices;

namespace Presentation.Models;

public class LoginViewModel
{
    [Required(ErrorMessage = Attributes.EmailRequired)]
    public string Email { get; init; }
    
    [Required(ErrorMessage = Attributes.PasswordRequired)]
    [DataType(DataType.Password)]
    public string Password { get; init; }
}