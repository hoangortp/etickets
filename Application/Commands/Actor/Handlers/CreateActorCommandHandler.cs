﻿using Application.Responses;
using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Actor.Handlers
{
    public class CreateActorCommandHandler : IRequestHandler<CreateActorCommand, ActorResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateActorCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ActorResponse> Handle(CreateActorCommand command, CancellationToken cancellationToken)
        {
            var actor = CreateActorCommand.Create(command);

            await _unitOfWork.ActorRepository.AddAsync(actor);
            await _unitOfWork.SaveChangesAsync();

            var response = _mapper.Map<ActorResponse>(actor);

            return response;
        }
    }
}
