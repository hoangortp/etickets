﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Application.Commands.Actor.Handlers
{
    public class DeleteActorCommandHandler : IRequestHandler<DeleteActorCommand, ActorResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<DeleteActorCommandHandler> _logger;

        public DeleteActorCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ActorResponse> Handle(DeleteActorCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var actor = await _unitOfWork.ActorRepository.GetAsync(actor => actor.Id == command.Id)
                            ?? throw new KeyNotFoundException(Exceptions.ActorNotFound);

                await _unitOfWork.ActorRepository.RemoveAsync(actor);
                await _unitOfWork.SaveChangesAsync();

                var response = ActorResponse.Create(actor);

                return response;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Actor with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while deleting the actor with Id {id}", command.Id);
                throw;
            }
        }
    }
}
