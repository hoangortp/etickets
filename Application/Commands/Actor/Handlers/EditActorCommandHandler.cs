﻿using Application.Responses;
using AutoMapper;
using Domain.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Actor.Handlers
{
    public class EditActorCommandHandler : IRequestHandler<EditActorCommand, ActorResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<EditActorCommandHandler> _logger;

        public EditActorCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ILogger<EditActorCommandHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ActorResponse> Handle(EditActorCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var actor = await _unitOfWork.ActorRepository.GetAsync(actor => actor.Id == command.Id)
                            ?? throw new KeyNotFoundException(Exceptions.ActorNotFound);

                actor.ProfilePictureUrl = command.ProfilePictureUrl ?? actor.ProfilePictureUrl;
                actor.FullName = command.FullName ?? actor.FullName;
                actor.Bio = command.Bio ?? actor.Bio;

                await _unitOfWork.ActorRepository.UpdateAsync(actor);
                await _unitOfWork.SaveChangesAsync();

                var actorResponse = _mapper.Map<ActorResponse>(actor);

                return actorResponse;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Actor with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while editing the actor with Id {id}", command.Id);
                throw;
            }
        }
    }
}
