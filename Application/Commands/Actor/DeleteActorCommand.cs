﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Commands.Actor
{
    public class DeleteActorCommand : IRequest<ActorResponse>
    {
        public Guid Id { get; set; }
    }
}
