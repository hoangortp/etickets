﻿using Application.Responses;
using Core.Enums;
using Domain.Constants;
using MediatR;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using System.Linq;

namespace Application.Commands.Movie
{
    public class EditMovieCommand : IRequest<MovieResponse>
    {
        public Guid Id { get; set; }

        [Display(Name = Attributes.MovieName)]
        [Required(ErrorMessage = Attributes.NameRequired)]
        public string Name { get; init; }

        [Display(Name = Attributes.MovieDescription)]
        [Required(ErrorMessage = Attributes.DescriptionRequired)]
        public string Description { get; init; }

        [Display(Name = Attributes.MoviePrice)]
        [Required(ErrorMessage = Attributes.PriceRequired)]
        public double Price { get; init; }

        [Display(Name = Attributes.MoviePosterUrl)]
        [Required(ErrorMessage = Attributes.MoviePosterUrlRequired)]
        public string ImageUrl { get; init; }

        [Display(Name = Attributes.StartDate)]
        [Required(ErrorMessage = Attributes.StartDateRequired)]
        public DateTime StartDate { get; init; }

        [Display(Name = Attributes.EndDate)]
        [Required(ErrorMessage = Attributes.EndDateRequired)]
        public DateTime EndDate { get; init; }

        [Display(Name = Attributes.CategorySelect)]
        [Required(ErrorMessage = Attributes.CategoryRequired)]
        public MovieCategory MovieCategory { get; init; }

        [Display(Name = Attributes.ActorsSelect)]
        [Required(ErrorMessage = Attributes.ActorsRequired)]
        public ICollection<Guid> ActorIds { get; init; }

        [Display(Name = Attributes.CinemaSelect)]
        [Required(ErrorMessage = Attributes.CinemaRequired)]
        public Guid CinemaId { get; init; }

        [Display(Name = Attributes.ProducerSelect)]
        [Required(ErrorMessage = Attributes.ProducerRequired)]
        public Guid ProducerId { get; init; }

        public static EditMovieCommand Create(MovieResponse response)
        {
            if (response is null)
            {
                return null;
            }

            var comamnd = new EditMovieCommand
            {
                Id = response.Id,
                Name = response.Name,
                Description = response.Description,
                Price = response.Price,
                ImageUrl = response.ImageUrl,
                StartDate = response.StartDate,
                EndDate = response.EndDate,
                MovieCategory = response.MovieCategory,
                ActorIds = response.ActorsMovies.Select(x => x.ActorId).ToList(),
                CinemaId = response.CinemaId,
                ProducerId = response.ProducerId,
            };

            return comamnd;
        }
    }
}
