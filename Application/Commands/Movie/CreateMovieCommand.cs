﻿using Application.Responses;
using Core.Enums;
using Domain.Constants;
using MediatR;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;

namespace Application.Commands.Movie
{
    public class CreateMovieCommand : IRequest<MovieResponse>
    {
        [Display(Name = Attributes.MovieName)]
        [Required(ErrorMessage = Attributes.NameRequired)]
        public string Name { get; init; }

        [Display(Name = Attributes.MovieDescription)]
        [Required(ErrorMessage = Attributes.DescriptionRequired)]
        public string Description { get; init; }

        [Display(Name = Attributes.MoviePrice)]
        [Required(ErrorMessage = Attributes.PriceRequired)]
        public double Price { get; init; }

        [Display(Name = Attributes.MoviePosterUrl)]
        [Required(ErrorMessage = Attributes.MoviePosterUrlRequired)]
        public string ImageUrl { get; init; }

        [Display(Name = Attributes.StartDate)]
        [Required(ErrorMessage = Attributes.StartDateRequired)]
        public DateTime StartDate { get; init; }

        [Display(Name = Attributes.EndDate)]
        [Required(ErrorMessage = Attributes.EndDateRequired)]
        public DateTime EndDate { get; init; }

        [Display(Name = Attributes.CategorySelect)]
        [Required(ErrorMessage = Attributes.CategoryRequired)]
        public MovieCategory MovieCategory { get; init; }

        [Display(Name = Attributes.ActorsSelect)]
        [Required(ErrorMessage = Attributes.ActorsRequired)]
        public ICollection<Guid> ActorIds { get; init; }

        [Display(Name = Attributes.CinemaSelect)]
        [Required(ErrorMessage = Attributes.CinemaRequired)]
        public Guid CinemaId { get; init; }

        [Display(Name = Attributes.ProducerSelect)]
        [Required(ErrorMessage = Attributes.ProducerRequired)]
        public Guid ProducerId { get; init; }

        public static Domain.Entities.Movie Create(CreateMovieCommand command)
        {
            if (command is null)
            {
                return null;
            }

            var movie = new Domain.Entities.Movie
            {
                Name = command.Name,
                Description = command.Description,
                Price = command.Price,
                ImageUrl = command.ImageUrl,
                StartDate = command.StartDate,
                EndDate = command.EndDate,
                MovieCategory = command.MovieCategory,
                CinemaId = command.CinemaId,
                ProducerId = command.ProducerId,
            };

            return movie;
        }
    }
}
