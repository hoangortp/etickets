﻿using Application.Responses;
using Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Movie.Handlers
{
    public class CreateMovieCommandHandler : IRequestHandler<CreateMovieCommand, MovieResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateMovieCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<MovieResponse> Handle(CreateMovieCommand command, CancellationToken cancellationToken)
        {
            var movie = CreateMovieCommand.Create(command);

            await _unitOfWork.MovieRepository.AddAsync(movie);

            foreach (var actorId in command.ActorIds)
            {
                var newActorMovie = new ActorMovie()
                {
                    MovieId = movie.Id,
                    ActorId = actorId
                };

                await _unitOfWork.ActorMovieRepository.AddAsync(newActorMovie);
            }

            await _unitOfWork.SaveChangesAsync();

            var response = MovieResponse.Create(movie);

            return response;
        }
    }
}
