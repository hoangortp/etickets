﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Application.Commands.Movie.Handlers
{
    public class DeleteMovieCommandHandler : IRequestHandler<DeleteMovieCommand, MovieResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<DeleteMovieCommandHandler> _logger;

        public DeleteMovieCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<MovieResponse> Handle(DeleteMovieCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var movie = await _unitOfWork.MovieRepository.GetAsync(filter: m => m.Id == command.Id, tracked: true)
                            ?? throw new KeyNotFoundException(Exceptions.MovieNotFound);

                await _unitOfWork.MovieRepository.RemoveAsync(movie);
                await _unitOfWork.SaveChangesAsync();

                var response = MovieResponse.Create(movie);

                return response;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Movie with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occured while deleting the movie with Id {id}", command.Id);
                throw;
            }
        }
    }
}
