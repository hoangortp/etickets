﻿using Application.Responses;
using Domain.Constants;
using Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Movie.Handlers
{
    public class EditMovieCommandHandler : IRequestHandler<EditMovieCommand, MovieResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EditMovieCommandHandler> _logger;

        public EditMovieCommandHandler(IUnitOfWork unitOfWork, ILogger<EditMovieCommandHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<MovieResponse> Handle(EditMovieCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var movie = await _unitOfWork.MovieRepository.GetAsync(filter: m => m.Id == command.Id, tracked: true)
                            ?? throw new KeyNotFoundException(Exceptions.MovieNotFound);

                movie.Name = command.Name;
                movie.Description = command.Description;
                movie.Price = command.Price;
                movie.ImageUrl = command.ImageUrl;
                movie.CinemaId = command.CinemaId;
                movie.StartDate = command.StartDate;
                movie.EndDate = command.EndDate;
                movie.MovieCategory = command.MovieCategory;
                movie.ProducerId = command.ProducerId;

                await _unitOfWork.ActorMovieRepository.DeleteActorsMoviesByMovieIdAsync(command.Id);
                await _unitOfWork.MovieRepository.UpdateAsync(movie);

                foreach (var actorId in command.ActorIds)
                {
                    var newActorMovie = new ActorMovie()
                    {
                        MovieId = movie.Id,
                        ActorId = actorId
                    };

                    await _unitOfWork.ActorMovieRepository.AddAsync(newActorMovie);
                }

                await _unitOfWork.SaveChangesAsync();

                var movieResponse = MovieResponse.Create(movie);

                return movieResponse;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Movie with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while editing movie with Id {id}", command.Id);
                throw;
            }
        }
    }
}
