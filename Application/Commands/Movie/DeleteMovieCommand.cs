﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Commands.Movie
{
    public class DeleteMovieCommand : IRequest<MovieResponse>
    {
        public Guid Id{ get; set; }
    }
}
