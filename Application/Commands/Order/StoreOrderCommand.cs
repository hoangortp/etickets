﻿using Application.Responses;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace Application.Commands.Order
{
    public class StoreOrderCommand : IRequest<OrderResponse>
    {
        public Guid UserId { get; set; }

        public string Email { get; set; }

        public List<ShoppingCartItem> Items { get; set; }

        public StoreOrderCommand(List<ShoppingCartItem> items, Guid userId, string email)
        {
            Items = items;
            UserId = userId;
            Email = email;
        }

        public static Domain.Aggregates.OrderAggregate.Order Create(StoreOrderCommand command)
        {
            if (command is null)
            {
                return null;
            }

            var order = new Domain.Aggregates.OrderAggregate.Order
            {
                UserId = command.UserId,
                Email = command.Email,
            };

            return order;
        }
    }
}
