﻿using Application.Responses;
using Domain.Aggregates.OrderAggregate;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Order.Handlers
{
    public class StoreOrderCommandHandler : IRequestHandler<StoreOrderCommand, OrderResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public StoreOrderCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<OrderResponse> Handle(StoreOrderCommand command, CancellationToken cancellationToken)
        {
            var order = StoreOrderCommand.Create(command);

            await _unitOfWork.OrderRepository.AddAsync(order);

            foreach (var orderItem in command.Items.Select(item => new OrderItem()
            {
                Amount = item.Amount,
                MovieId = item.MovieId,
                OrderId = order.Id,
                Price = item.Movie.Price,
            }))
            {
                await _unitOfWork.OrderItemRepository.AddAsync(orderItem);
            }

            await _unitOfWork.SaveChangesAsync();

            var response = OrderResponse.Create(order);

            return response;
        }
    }
}
