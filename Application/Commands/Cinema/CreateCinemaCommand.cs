﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.Commands.Cinema
{
    public class CreateCinemaCommand : IRequest<CinemaResponse>
    {
        [Required(ErrorMessage = Attributes.LogoRequired)]
        public string Logo { get; init; }

        [Required(ErrorMessage = Attributes.NameRequired)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = Attributes.NameLimitation)]
        public string Name { get; init; }

        [Required(ErrorMessage = Attributes.DescriptionRequired)]
        public string Description { get; init; }

        public static Domain.Entities.Cinema Create(CreateCinemaCommand command)
        {
            if (command is null)
            {
                return null;
            }

            var cinema = new Domain.Entities.Cinema
            {
                Logo = command.Logo,
                Name = command.Name,
                Description = command.Description,
            };

            return cinema;
        }
    }
}
