﻿using Application.Responses;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Cinema.Handlers
{
    public class CreateCinemaCommandHandler : IRequestHandler<CreateCinemaCommand, CinemaResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateCinemaCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CinemaResponse> Handle(CreateCinemaCommand command, CancellationToken cancellationToken)
        {
            var cinema = CreateCinemaCommand.Create(command);

            await _unitOfWork.CinemaRepository.AddAsync(cinema);
            await _unitOfWork.SaveChangesAsync();

            var response = CinemaResponse.Create(cinema);

            return response;
        }
    }
}
