﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Cinema.Handlers
{
    public class EditCinemaCommandHandler : IRequestHandler<EditCinemaCommand, CinemaResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EditCinemaCommandHandler> _logger;

        public EditCinemaCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CinemaResponse> Handle(EditCinemaCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var cinema = await _unitOfWork.CinemaRepository.GetAsync(cinema => cinema.Id == command.Id)
                             ?? throw new KeyNotFoundException(Exceptions.CinemaNotFound);

                cinema.Logo = command.Logo ?? cinema.Logo;
                cinema.Name = command.Name ?? cinema.Name;
                cinema.Description = command.Description ?? cinema.Description;

                await _unitOfWork.CinemaRepository.UpdateAsync(cinema);
                await _unitOfWork.SaveChangesAsync();

                var response = CinemaResponse.Create(cinema);

                return response;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Cinema with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occured while editing the cinema with Id {id}", command.Id);
                throw;
            }
        }
    }
}
