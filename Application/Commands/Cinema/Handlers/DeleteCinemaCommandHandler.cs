﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Application.Commands.Cinema.Handlers
{
    public class DeleteCinemaCommandHandler : IRequestHandler<DeleteCinemaCommand, CinemaResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<DeleteCinemaCommandHandler> _logger;   

        public DeleteCinemaCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CinemaResponse> Handle(DeleteCinemaCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var cinema = await _unitOfWork.CinemaRepository.GetAsync(cinema => cinema.Id == command.Id)
                             ?? throw new KeyNotFoundException(Exceptions.CinemaNotFound);

                await _unitOfWork.CinemaRepository.RemoveAsync(cinema);
                await _unitOfWork.SaveChangesAsync();

                var response = CinemaResponse.Create(cinema);

                return response;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Cinema with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while deleting the cinema with Id {id}", command.Id);
                throw;
            }
        }
    }
}
