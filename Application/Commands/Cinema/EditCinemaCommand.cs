﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;

namespace Application.Commands.Cinema
{
    public class EditCinemaCommand : IRequest<CinemaResponse>
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = Attributes.LogoRequired)]
        public string Logo { get; init; }

        [Required(ErrorMessage = Attributes.NameLimitation)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = Attributes.NameLimitation)]
        public string Name { get; init; }

        [Required(ErrorMessage = Attributes.DescriptionRequired)]
        public string Description { get; init; }

        public static EditCinemaCommand Create(CinemaResponse response)
        {
            if (response is null)
            {
                return null;
            }

            var command = new EditCinemaCommand
            {
                Id = response.Id,
                Logo = response.Logo,
                Name = response.Name,
                Description = response.Description,
            };

            return command;
        }
    }
}
