﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Commands.Cinema
{
    public class DeleteCinemaCommand : IRequest<CinemaResponse>
    {
        public Guid Id { get; set; }
    }
}
