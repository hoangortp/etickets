﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.Commands.Producer
{
    public class CreateProducerCommand : IRequest<ProducerResponse>
    {
        [Display(Name = Attributes.ProfilePicture)]
        [Required(ErrorMessage = Attributes.ProfilePictureRequired)]
        public string ProfilePictureUrl { get; init; }

        [Display(Name = Attributes.FullName)]
        [Required(ErrorMessage = Attributes.FullNameRequired)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = Attributes.FullNameLimitation)]
        public string FullName { get; init; }

        [Display(Name = Attributes.Biography)]
        [Required(ErrorMessage = Attributes.BiographyRequired)]
        public string Bio { get; init; }

        public static Domain.Entities.Producer Create(CreateProducerCommand command)
        {
            if (command is null)
            {
                return null;
            }

            var producer = new Domain.Entities.Producer
            {
                ProfilePictureUrl = command.ProfilePictureUrl,
                FullName = command.FullName,
                Bio = command.Bio,
            };

            return producer;
        }
    }
}
