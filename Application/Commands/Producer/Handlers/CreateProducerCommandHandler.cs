﻿using Application.Responses;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Producer.Handlers
{
    public class CreateProducerCommandHandler : IRequestHandler<CreateProducerCommand, ProducerResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateProducerCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ProducerResponse> Handle(CreateProducerCommand command, CancellationToken cancellationToken)
        {
            var producer = CreateProducerCommand.Create(command);

            await _unitOfWork.ProducerRepository.AddAsync(producer);
            await _unitOfWork.SaveChangesAsync();

            var response = ProducerResponse.Create(producer);

            return response;
        }
    }
}
