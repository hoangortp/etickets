﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Application.Commands.Producer.Handlers
{
    public class DeleteProducerCommandHandler : IRequestHandler<DeleteProducerCommand, ProducerResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<DeleteProducerCommandHandler> _logger;

        public DeleteProducerCommandHandler(IUnitOfWork unitOfWork, ILogger<DeleteProducerCommandHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<ProducerResponse> Handle(DeleteProducerCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var producer = await _unitOfWork.ProducerRepository.GetAsync(producer => producer.Id == command.Id)
                               ?? throw new KeyNotFoundException(Exceptions.ProducerNotFound);

                await _unitOfWork.ProducerRepository.RemoveAsync(producer);
                await _unitOfWork.SaveChangesAsync();

                var response = ProducerResponse.Create(producer);
                
                return response;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Producer with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AN error occured while deleting the producer with Id {id}", command.Id);
                throw;
            }
        }
    }
}
