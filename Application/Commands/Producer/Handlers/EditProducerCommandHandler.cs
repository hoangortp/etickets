﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Commands.Producer.Handlers
{
    public class EditProducerCommandHandler : IRequestHandler<EditProducerCommand, ProducerResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EditProducerCommandHandler> _logger;

        public EditProducerCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ProducerResponse> Handle(EditProducerCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var producer = await _unitOfWork.ProducerRepository.GetAsync(producer => producer.Id == command.Id)
                               ?? throw new KeyNotFoundException(Exceptions.ProducerNotFound);

                producer.FullName = command.FullName ?? producer.FullName;
                producer.ProfilePictureUrl = command.ProfilePictureUrl ?? producer.ProfilePictureUrl;
                producer.Bio = command.Bio ?? producer.Bio;

                await _unitOfWork.ProducerRepository.UpdateAsync(producer);
                await _unitOfWork.SaveChangesAsync();

                var response = ProducerResponse.Create(producer);

                return response;
            }
            catch (KeyNotFoundException e)
            {
                _logger.LogWarning(e, "Producer with Id {id} was not found", command.Id);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while editing the producer with Id {id}", command.Id);
                throw;
            }
        }
    }
}
