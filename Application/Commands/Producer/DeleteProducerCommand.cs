﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Commands.Producer
{
    public class DeleteProducerCommand : IRequest<ProducerResponse>
    {
        public Guid Id { get; set; }
    }
}
