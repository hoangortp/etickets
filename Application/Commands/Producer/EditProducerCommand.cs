﻿using Application.Responses;
using Domain.Constants;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;

namespace Application.Commands.Producer
{
    public class EditProducerCommand : IRequest<ProducerResponse>
    {
        public Guid Id { get; set; }

        [Display(Name = Attributes.ProfilePicture)]
        [Required(ErrorMessage = Attributes.ProfilePictureRequired)]
        public string ProfilePictureUrl { get; init; }

        [Display(Name = Attributes.FullName)]
        [Required(ErrorMessage = Attributes.FullNameRequired)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = Attributes.FullNameLimitation)]
        public string FullName { get; init; }

        [Display(Name = Attributes.Biography)]
        [Required(ErrorMessage = Attributes.BiographyRequired)]
        public string Bio { get; init; }

        public static EditProducerCommand Create(ProducerResponse response)
        {
            if (response is null)
            {
                return null;
            }

            var command = new EditProducerCommand
            {
                Id = response.Id,
                ProfilePictureUrl = response.ProfilePictureUrl,
                FullName = response.FullName,
                Bio = response.Bio,
            };

            return command;
        }

    }
}
