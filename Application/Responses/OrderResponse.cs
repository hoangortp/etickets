using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Domain.Aggregates.IdentityAggregate;
using Domain.Aggregates.OrderAggregate;

namespace Application.Responses;

public class OrderResponse
{
    #region Properties

    public Guid Id { get; set; }

    public string Email { get; set; }

    public Guid UserId { get; set; }

    public List<OrderItem> OrderItems { get; set; }

    public ApplicationUser User { get; set; }

    #endregion

    public static Expression<Func<Order, OrderResponse>> Projection
    {
        get
        {
            return value => new OrderResponse
            {
                Id = value.Id,
                Email = value.Email,
                User = value.User,
            };
        }
    }

    public static OrderResponse Create(Order order)
    {
        if (order is null)
        {
            return null;
        }

        return Projection.Compile().Invoke(order);
    }
}