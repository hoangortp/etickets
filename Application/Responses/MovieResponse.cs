﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Core.Enums;
using Domain.Entities;

namespace Application.Responses;

public class MovieResponse
{
	#region Properties

	public Guid Id { get; set; }

	public string Name { get; set; }

	public string Description { get; set; }

	public double Price { get; set; }

	public string ImageUrl { get; set; }

	public DateTime StartDate { get; set; }

	public DateTime EndDate { get; set; }

	public MovieCategory MovieCategory { get; set; }

	public ICollection<Guid> ActorIds { get; set; }

	public ICollection<ActorMovie> ActorsMovies { get; set; }

	public Guid CinemaId { get; set; }

	public Cinema Cinema { get; set; }

	public Guid ProducerId { get; set; }

	public Producer Producer { get; set; }

	#endregion

	public static Expression<Func<Domain.Entities.Movie, MovieResponse>> Projection
	{
		get
		{
			return value => new MovieResponse
			{
				Id = value.Id,
				Name = value.Name,
				Description = value.Description,
				Price = value.Price,
				ImageUrl = value.ImageUrl,
				StartDate = value.StartDate,
				EndDate = value.EndDate,
				MovieCategory = value.MovieCategory,
				ActorsMovies = value.ActorsMovies,
				CinemaId = value.CinemaId,
				Cinema = value.Cinema,
				ProducerId = value.ProducerId,
				Producer = value.Producer,
			};
		}
	}

	public static MovieResponse Create(Domain.Entities.Movie movie)
	{
		if (movie is null)
		{
			return null;
		}

		return Projection.Compile().Invoke(movie);
	}
}