﻿using System;
using System.Linq.Expressions;

namespace Application.Responses;

public class ProducerResponse
{
    #region Properties

    public Guid Id { get; init; }
    
    
    public string ProfilePictureUrl { get; init; }
    
    
    public string FullName { get; init; }
    
    
    public string Bio { get; init; }

    #endregion

    public static Expression<Func<Domain.Entities.Producer, ProducerResponse>> Projection
    {
        get
        {
            return value => new ProducerResponse
            {
                Id = value.Id,
                ProfilePictureUrl = value.ProfilePictureUrl,
                FullName = value.FullName,
                Bio = value.Bio,
            };
        }
    }

    public static ProducerResponse Create(Domain.Entities.Producer producer)
    {
        if (producer is null)
        {
            return null;
        }    

        return Projection.Compile().Invoke(producer);
    }
}