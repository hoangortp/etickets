using System.Collections.Generic;
using Domain.Entities;

namespace Application.Responses;

public class NewMovieDropdownsResponse
{
    #region Properties

    public ICollection<Producer> Producers { get; set; } = [];
    
    public ICollection<Cinema> Cinemas { get; set; } = [];
    
    public ICollection<Actor> Actors { get; set; } = [];

    #endregion

    public NewMovieDropdownsResponse(ICollection<Producer> producers, 
                                     ICollection<Cinema> cinemas, 
                                     ICollection<Actor> actors)
    {
        Producers = producers;
        Cinemas = cinemas;
        Actors = actors;
    }
}