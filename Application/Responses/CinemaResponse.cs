﻿using System;
using System.Linq.Expressions;

namespace Application.Responses;

public class CinemaResponse
{
    #region Properties

    public Guid Id { get; init; }
    
    public string Logo { get; init; }
    
    public string Name { get; init; }
    
    public string Description { get; init; }

    #endregion

    public static Expression<Func<Domain.Entities.Cinema, CinemaResponse>> Projection
    {
        get
        {
            return value => new CinemaResponse
            {
                Id = value.Id,
                Logo = value.Logo,
                Name = value.Name,
                Description = value.Description,
            };
        }
    }

    public static CinemaResponse Create(Domain.Entities.Cinema cinema)
    {
        if (cinema is null)
        {
            return null;
        }

        return Projection.Compile().Invoke(cinema);
    }
}