﻿using System;
using System.Linq.Expressions;

namespace Application.Responses;

public class ActorResponse
{
    #region Properties

    public Guid Id { get; init; }
    
    public string ProfilePictureUrl { get; init; }
    
    public string FullName { get; init; }
    
    public string Bio { get; init; }

    public static Expression<Func<Domain.Entities.Actor, ActorResponse>> Projection
    {
        get
        {
            return value => new ActorResponse
            {
                Id = value.Id,
                ProfilePictureUrl = value.ProfilePictureUrl,
                FullName = value.FullName,
                Bio = value.Bio,
            };
        }
    }

    public static ActorResponse Create(Domain.Entities.Actor actor)
    {
        if (actor is null)
        {
            return null;
        }

        return Projection.Compile().Invoke(actor);    
    }

    #endregion
}