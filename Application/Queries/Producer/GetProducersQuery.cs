﻿using Application.Responses;
using MediatR;
using System.Collections;
using System.Collections.Generic;

namespace Application.Queries.Producer
{
    public class GetProducersQuery : IRequest<ICollection<ProducerResponse>>
    {
    }
}
