﻿using Application.Responses;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Producer.Handlers
{
    public class GetProducersQueryHandler : IRequestHandler<GetProducersQuery, ICollection<ProducerResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetProducersQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ICollection<ProducerResponse>> Handle(GetProducersQuery query, CancellationToken cancellationToken)
        {
            var producers = await _unitOfWork.ProducerRepository.GetAllAsync();

            var response = _mapper.Map<ICollection<ProducerResponse>>(producers);

            return response;
        }
    }
}
