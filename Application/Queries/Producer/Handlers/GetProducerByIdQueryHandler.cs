﻿using Application.Responses;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Producer.Handlers
{
    public class GetProducerByIdQueryHandler : IRequestHandler<GetProducerByIdQuery, ProducerResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetProducerByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ProducerResponse> Handle(GetProducerByIdQuery query, CancellationToken cancellationToken)
        {
            var producer = await _unitOfWork.ProducerRepository.GetAsync(x => x.Id == query.Id);

            var response = ProducerResponse.Create(producer);

            return response;
        }
    }
}
