﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Queries.Producer
{
    public class GetProducerByIdQuery : IRequest<ProducerResponse>
    {
        public Guid Id { get; set; }

        public GetProducerByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
