﻿using Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace Application.Queries.Orders
{
    public class GetOrdersQuery : IRequest<List<OrderResponse>>
    {
        public Guid UserId { get; set; }

        public string Role { get; set; }

        public GetOrdersQuery(Guid userId, string role)
        {
            UserId = userId;
            Role = role;
        }
    }
}
