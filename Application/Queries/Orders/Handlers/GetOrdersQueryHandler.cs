﻿using Application.Responses;
using AutoMapper;
using Domain.Aggregates.IdentityAggregate;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Orders.Handlers
{
    internal class GetOrdersQueryHandler : IRequestHandler<GetOrdersQuery, List<OrderResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetOrdersQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<OrderResponse>> Handle(GetOrdersQuery query, CancellationToken cancellationToken)
        {
            var orders = await _unitOfWork.OrderRepository.GetOrdersAsync();

            if (query.Role != Roles.Admin)
            {
                orders = orders.Where(o => o.UserId == query.UserId).ToList();
            }

            var ordersResponse = _mapper.Map<List<OrderResponse>>(orders);

            return ordersResponse;
        }
    }
}
