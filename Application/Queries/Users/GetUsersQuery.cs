﻿using Domain.Aggregates.IdentityAggregate;
using MediatR;
using System.Collections.Generic;

namespace Application.Queries.Users
{
    public class GetUsersQuery : IRequest<ICollection<ApplicationUser>>
    {
    }
}
