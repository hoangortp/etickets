﻿using Domain.Aggregates.IdentityAggregate;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Users.Handlers
{
    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, ICollection<ApplicationUser>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetUsersQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ICollection<ApplicationUser>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await _unitOfWork.UserRepository.GetAllAsync();

            return users;
        }
    }
}
