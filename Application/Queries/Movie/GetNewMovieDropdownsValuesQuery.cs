﻿using Application.Responses;
using MediatR;

namespace Application.Queries.Movie
{
    public class GetNewMovieDropdownsValuesQuery : IRequest<NewMovieDropdownsResponse>
    {
    }
}
