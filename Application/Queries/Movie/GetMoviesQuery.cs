﻿using Application.Responses;
using MediatR;
using System.Collections.Generic;

namespace Application.Queries.Movie
{
    public class GetMoviesQuery : IRequest<ICollection<MovieResponse>>
    {
    }
}
