﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Queries.Movie
{
    public class GetMovieByIdQuery : IRequest<MovieResponse>
    {
        public Guid Id { get; set; }

        public GetMovieByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
