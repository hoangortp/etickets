﻿using Application.Responses;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Movie.Handlers
{
    public class GetNewMovieDropdownsValuesQueryHandler : IRequestHandler<GetNewMovieDropdownsValuesQuery, NewMovieDropdownsResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetNewMovieDropdownsValuesQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<NewMovieDropdownsResponse> Handle(GetNewMovieDropdownsValuesQuery request, CancellationToken cancellationToken)
        {
            var actors = await _unitOfWork.ActorRepository.GetAllAsync(orderBy: actors => actors.OrderBy(actor => actor.FullName));
            var cinemas = await _unitOfWork.CinemaRepository.GetAllAsync(orderBy: cinemas => cinemas.OrderBy(cinema => cinema.Name));
            var producers = await _unitOfWork.ProducerRepository.GetAllAsync(orderBy: producers => producers.OrderBy(producer => producer.FullName));

            var response = new NewMovieDropdownsResponse(producers, cinemas, actors);

            return response;
        }
    }
}
