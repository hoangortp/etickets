﻿using Application.Responses;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Movie.Handlers
{
    public class GetMovieByIdQueryHandler : IRequestHandler<GetMovieByIdQuery, MovieResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetMovieByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<MovieResponse> Handle(GetMovieByIdQuery query, CancellationToken cancellationToken)
        {
            var movie = await _unitOfWork.MovieRepository.GetAsync(m => m.Id == query.Id);

            MovieResponse movieResponse = MovieResponse.Create(movie);

            return movieResponse;
        }
    }
}
