﻿using Application.Responses;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Movie.Handlers
{
    public class GetMoviesQueryHandler : IRequestHandler<GetMoviesQuery, ICollection<MovieResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMoviesQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ICollection<MovieResponse>> Handle(GetMoviesQuery query, CancellationToken cancellationToken)
        {
            var movies = await _unitOfWork.MovieRepository.GetAllAsync(includeProperties: [movie => movie.Cinema]);

            var response = _mapper.Map<ICollection<MovieResponse>>(movies);

            return response;
        }
    }
}
