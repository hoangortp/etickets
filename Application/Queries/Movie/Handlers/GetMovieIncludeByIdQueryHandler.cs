﻿using Application.Responses;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Movie.Handlers
{
    public class GetMovieIncludeByIdQueryHandler : IRequestHandler<GetMovieIncludeByIdQuery, MovieResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetMovieIncludeByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<MovieResponse> Handle(GetMovieIncludeByIdQuery query, CancellationToken cancellationToken)
        {
            var movie = await _unitOfWork.MovieRepository.GetAndIncludeByIdAsync(query.Id);

            var response = MovieResponse.Create(movie);

            return response;
        }
    }
}
