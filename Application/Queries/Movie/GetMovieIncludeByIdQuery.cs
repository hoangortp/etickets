﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Queries.Movie
{
    public class GetMovieIncludeByIdQuery : IRequest<MovieResponse>
    {
        public Guid Id { get; set; }

        public GetMovieIncludeByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
