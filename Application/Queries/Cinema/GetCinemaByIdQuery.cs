﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Queries.Cinema
{
    public class GetCinemaByIdQuery : IRequest<CinemaResponse>
    {
        public Guid Id { get; set; }

        public GetCinemaByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
