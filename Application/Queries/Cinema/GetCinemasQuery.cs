﻿using Application.Responses;
using MediatR;
using System.Collections;
using System.Collections.Generic;

namespace Application.Queries.Cinema
{
    public class GetCinemasQuery : IRequest<ICollection<CinemaResponse>>
    {
    }
}
