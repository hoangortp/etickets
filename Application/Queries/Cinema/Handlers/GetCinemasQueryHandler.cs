﻿using Application.Responses;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Cinema.Handlers
{
    public class GetCinemasQueryHandler : IRequestHandler<GetCinemasQuery, ICollection<CinemaResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetCinemasQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ICollection<CinemaResponse>> Handle(GetCinemasQuery request, CancellationToken cancellationToken)
        {
            var actors = await _unitOfWork.CinemaRepository.GetAllAsync();

            var response = _mapper.Map<ICollection<CinemaResponse>>(actors);

            return response;
        }
    }
}
