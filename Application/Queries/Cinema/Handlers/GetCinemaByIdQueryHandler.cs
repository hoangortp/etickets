﻿using Application.Responses;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Cinema.Handlers
{
    public class GetCinemaByIdQueryHandler : IRequestHandler<GetCinemaByIdQuery, CinemaResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetCinemaByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CinemaResponse> Handle(GetCinemaByIdQuery query, CancellationToken cancellationToken)
        {
            var cinema = await _unitOfWork.CinemaRepository.GetAsync(cinema => cinema.Id == query.Id);

            var response = CinemaResponse.Create(cinema);

            return response;
        }
    }
}
