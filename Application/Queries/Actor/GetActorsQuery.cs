﻿using Application.Responses;
using MediatR;
using System.Collections.Generic;

namespace Application.Queries.Actor
{
    public class GetActorsQuery : IRequest<ICollection<ActorResponse>>
    {
    }
}
