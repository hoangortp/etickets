﻿using Application.Responses;
using MediatR;
using System;

namespace Application.Queries.Actor
{
    public class GetActorByIdQuery : IRequest<ActorResponse>
    {
        public Guid Id { get; set; }

        public GetActorByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
