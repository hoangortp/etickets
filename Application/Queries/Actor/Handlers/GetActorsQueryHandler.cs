﻿using Application.Responses;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Actor.Handlers
{
    public class GetActorsQueryHandler : IRequestHandler<GetActorsQuery, ICollection<ActorResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetActorsQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ICollection<ActorResponse>> Handle(GetActorsQuery query, CancellationToken cancellationToken)
        {
            var actors = await _unitOfWork.ActorRepository.GetAllAsync();

            var response = _mapper.Map<ICollection<ActorResponse>>(actors);

            return response;
        }
    }
}
