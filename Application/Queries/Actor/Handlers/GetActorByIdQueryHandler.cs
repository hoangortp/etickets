﻿using Application.Responses;
using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.Actor.Handlers
{
    public class GetActorByIdQueryHandler : IRequestHandler<GetActorByIdQuery, ActorResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetActorByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ActorResponse> Handle(GetActorByIdQuery query, CancellationToken cancellationToken)
        {
            var actor = await _unitOfWork.ActorRepository.GetAsync(actor => actor.Id == query.Id);

            var response = ActorResponse.Create(actor);

            return response;
        }
    }
}
