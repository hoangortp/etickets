﻿using Domain.Repositories;
using System.Threading.Tasks;
using Domain.Aggregates.IdentityAggregate;
using Domain.Aggregates.OrderAggregate;

namespace Application;

public interface IUnitOfWork
{
	#region Properties

	IActorRepository ActorRepository { get; }

	ICinemaRepository CinemaRepository { get; }

	IMovieRepository MovieRepository { get; }

	IProducerRepository ProducerRepository { get; }

	IActorMovieRepository ActorMovieRepository { get; }
	
	IShoppingCartItemRepository ShoppingCartItemRepository { get;  }
	
	IOrderRepository OrderRepository { get; }
	
	IOrderItemRepository OrderItemRepository { get; }
	
	IUserRepository UserRepository { get; }

	#endregion

	#region Methods

	Task SaveChangesAsync();

	#endregion
}