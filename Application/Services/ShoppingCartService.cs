using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Services;

public class ShoppingCartService
{
    #region Fields

    private readonly IUnitOfWork _unitOfWork;

    #endregion

    #region Properties

    public string ShoppingCartId { get; set; }

    public List<ShoppingCartItem> ShoppingCartItems { get; set; }

    #endregion

    #region Constructors

    public ShoppingCartService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    #endregion

    #region Methods

    public ShoppingCartService GetShoppingCart(IServiceProvider services)
    {
        string cartIdString = "Cart Id";

        ISession session = services.GetRequiredService<IHttpContextAccessor>().HttpContext?.Session;

        string cartId = session?.GetString(cartIdString) ?? Guid.NewGuid().ToString();

        session?.SetString(cartIdString, cartId);

        return new ShoppingCartService(_unitOfWork)
        {
            ShoppingCartId = cartId,
        };
    }

    public async Task AddItemToCartAsync(Guid id)
    {
        var shoppingCartItem = await _unitOfWork.ShoppingCartItemRepository
                                                .GetByMovieIdAndShoppingCartIdAsync(id, ShoppingCartId);

        if (shoppingCartItem is null)
        {
            shoppingCartItem = new ShoppingCartItem()
            {
                ShoppingCartId = ShoppingCartId,
                MovieId = id,
                Amount = 1
            };

            await _unitOfWork.ShoppingCartItemRepository.AddAsync(shoppingCartItem);
        }
        else
        {
            shoppingCartItem.Amount++;
        }

        await _unitOfWork.SaveChangesAsync();
    }

    public async Task RemoveItemFromCartAsync(Guid id)
    {
        var shoppingCartItem = await _unitOfWork.ShoppingCartItemRepository
                                                .GetByMovieIdAndShoppingCartIdAsync(id, ShoppingCartId);

        if (shoppingCartItem is not null)
        {
            if (shoppingCartItem.Amount > 1)
            {
                shoppingCartItem.Amount--;
            }
            else
            {
                await _unitOfWork.ShoppingCartItemRepository.RemoveAsync(shoppingCartItem);
            }
        }

        await _unitOfWork.SaveChangesAsync();
    }

    public async Task<List<ShoppingCartItem>> GetShoppingCartItemsAsync()
    {
        return ShoppingCartItems ??= await _unitOfWork.ShoppingCartItemRepository
                                                      .GetAllByShoppingCartIdIncludeMovieAsync(ShoppingCartId);
    }

    public async Task<double> GetShoppingCartTotalAsync()
    {
        return await _unitOfWork.ShoppingCartItemRepository.GetTotalAsync(ShoppingCartId);
    }

    public async Task ClearShoppingCartAsync()
    {
        await _unitOfWork.ShoppingCartItemRepository.ClearShoppingCartAsync(ShoppingCartId);
        await _unitOfWork.SaveChangesAsync();

        ShoppingCartItems = [];
    }

    #endregion
}