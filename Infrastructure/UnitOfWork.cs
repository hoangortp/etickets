﻿using System.Threading.Tasks;
using Application;
using Domain.Aggregates.IdentityAggregate;
using Domain.Aggregates.OrderAggregate;
using Domain.Repositories;
using Infrastructure.Contexts;
using Infrastructure.Repositories;

namespace Infrastructure;

public class UnitOfWork : IUnitOfWork
{
	#region Fields

	private readonly AppDbContext _context;

	#endregion

	#region Constructors

	public UnitOfWork(AppDbContext context)
	{
		_context = context;
		ActorRepository = new ActorRepository(_context);
		CinemaRepository = new CinemaRepository(_context);
		MovieRepository = new MovieRepository(_context);
		ProducerRepository = new ProducerRepository(_context);
		ActorMovieRepository = new ActorMovieRepository(_context);
		ShoppingCartItemRepository = new ShoppingCartItemRepository(_context);
		OrderRepository = new OrderRepository(_context);
		OrderItemRepository = new OrderItemRepository(_context);
		UserRepository = new UserRepository(_context);
	}

	#endregion

	#region Properties

	public IActorRepository ActorRepository { get; }

	public ICinemaRepository CinemaRepository { get; }

	public IMovieRepository MovieRepository { get; }

	public IProducerRepository ProducerRepository { get; }

	public IActorMovieRepository ActorMovieRepository { get; }
	
	public IShoppingCartItemRepository ShoppingCartItemRepository { get;  }
	
	public IOrderRepository OrderRepository { get; }

	public IOrderItemRepository OrderItemRepository { get; }	
	
	public IUserRepository UserRepository { get; }

	#endregion

	#region Methods

	/// <summary>
	/// Save change of entities if there is no error, else nothing changes
	/// </summary>
	public async Task SaveChangesAsync() => await _context.SaveChangesAsync();

	#endregion
}