﻿using Application.Responses;
using AutoMapper;
using Domain.Aggregates.OrderAggregate;
using Domain.Entities;

namespace Infrastructure.Mapping;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        #region Entity - Response

        CreateMap<Actor, ActorResponse>().ReverseMap();
        CreateMap<Cinema, CinemaResponse>().ReverseMap();
        CreateMap<Movie, MovieResponse>().ReverseMap();
        CreateMap<Producer, ProducerResponse>().ReverseMap();
        CreateMap<Order, OrderResponse>().ReverseMap();

        #endregion
    }
}