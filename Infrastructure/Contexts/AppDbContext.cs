﻿using System;
using Domain.Aggregates.IdentityAggregate;
using Domain.Aggregates.OrderAggregate;
using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Contexts;

/// <summary>
/// Setup and configure entities for creating database
/// </summary>
/// <param name="options">
/// Customize DbContext behavior, including database provider and connection details
/// </param>
public class AppDbContext(DbContextOptions<AppDbContext> options) : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>(options)
{
	#region Properties

	public DbSet<Actor> Actors { get; set; }

	public DbSet<Movie> Movies { get; set; }

	public DbSet<ActorMovie> ActorsMovies { get; set; }

	public DbSet<Cinema> Cinemas { get; set; }

	public DbSet<Producer> Producers { get; set; }

	public DbSet<Order> Orders { get; set; }

	public DbSet<OrderItem> OrderItems { get; set; }

	public DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }

	#endregion

	#region Methods

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.Entity<ActorMovie>().HasKey(am => new { am.ActorId, am.MovieId });

		modelBuilder.Entity<ActorMovie>().HasOne(am => am.Movie)
										 .WithMany(m => m.ActorsMovies)
										 .HasForeignKey(am => am.MovieId);

		modelBuilder.Entity<ActorMovie>().HasOne(am => am.Actor)
										 .WithMany(a => a.ActorsMovies)
										 .HasForeignKey(am => am.ActorId);

		base.OnModelCreating(modelBuilder);
	}

	#endregion
}