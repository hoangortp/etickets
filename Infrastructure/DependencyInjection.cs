﻿using Application;
using Domain.Aggregates.IdentityAggregate;
using Domain.Aggregates.OrderAggregate;
using Domain.Repositories;
using Infrastructure.Contexts;
using Infrastructure.Mapping;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure;

	public static class DependencyInjection
	{
		public static void AddInfrastructureServices(this IServiceCollection services, string databaseConnection)
		{
			// UnitOfWork
			services.AddScoped<IUnitOfWork, UnitOfWork>();

			// Repositories
			services.AddScoped<IActorRepository, ActorRepository>();
			services.AddScoped<IProducerRepository, ProducerRepository>();
			services.AddScoped<ICinemaRepository, CinemaRepository>();
			services.AddScoped<IMovieRepository, MovieRepository>();
			services.AddScoped<IShoppingCartItemRepository, ShoppingCartItemRepository>();
			services.AddScoped<IOrderRepository, OrderRepository>();
			services.AddScoped<IOrderItemRepository, OrderItemRepository>();
			services.AddScoped<IUserRepository, UserRepository>();
			services.AddScoped<IActorMovieRepository, ActorMovieRepository>();

			// DbContext
			services.AddDbContext<AppDbContext>(options => options.UseSqlServer(databaseConnection));

			// AutoMapper
			services.AddAutoMapper(typeof(MappingProfile).Assembly);
			

		}
	}
