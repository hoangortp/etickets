using System.Threading.Tasks;
using Domain.Aggregates.IdentityAggregate;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure;

public static class AppDbInitializer
{
    public static async Task SeedUserAndRolesAsync(this IApplicationBuilder builder)
    {
        using var serviceScope = builder.ApplicationServices.CreateScope();

        #region Roles

        var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

        if (!await roleManager.RoleExistsAsync(Roles.Admin))
        {
            await roleManager.CreateAsync(new ApplicationRole() { Name = Roles.Admin });
        }

        if (!await roleManager.RoleExistsAsync(Roles.User))
        {
            await roleManager.CreateAsync(new ApplicationRole() { Name = Roles.User });
        }

        #endregion

        #region Users

        const string adminEmail = "admin@etickets.com";
        const string adminUsername = "admin";
        const string adminPassword = "@Admin123";

        const string appUserEmail = "user@etickets.com";
        const string appUserUsername = "user";
        const string appUserPassword = "@User123";

        var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

        // Admin
        var adminUser = await userManager.FindByEmailAsync(adminEmail);

        if (adminUser == null)
        {
            var newAdminUser = new ApplicationUser()
            {
                FullName = "Admin",
                UserName = adminUsername,
                Email = adminEmail,
                EmailConfirmed = true,
            };

            await userManager.CreateAsync(newAdminUser, adminPassword);
            await userManager.AddToRoleAsync(newAdminUser, Roles.Admin);
        }

        // User
        var appUser = await userManager.FindByEmailAsync(appUserEmail);

        if (appUser == null)
        {
            var newAdminUser = new ApplicationUser()
            {
                FullName = "Application User",
                UserName = appUserUsername,
                Email = appUserEmail,
                EmailConfirmed = true,
            };

            await userManager.CreateAsync(newAdminUser, appUserPassword);
            await userManager.AddToRoleAsync(newAdminUser, Roles.User);
        }

        #endregion
    }
}