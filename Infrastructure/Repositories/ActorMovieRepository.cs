﻿using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories;

public class ActorMovieRepository(AppDbContext context) : BaseRepository<ActorMovie>(context), IActorMovieRepository
{
	#region Methods

	public async Task DeleteActorsMoviesByMovieIdAsync(Guid movieId)
	{
		await DbSet.Where(a => a.MovieId == movieId).ExecuteDeleteAsync();
		await Context.SaveChangesAsync();
	}

	#endregion
}