using Domain.Aggregates.IdentityAggregate;
using Infrastructure.Contexts;

namespace Infrastructure.Repositories;

public class UserRepository(AppDbContext context) : BaseRepository<ApplicationUser>(context), IUserRepository
{
    
}