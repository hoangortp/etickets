﻿using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Contexts;

namespace Infrastructure.Repositories;

public class CinemaRepository(AppDbContext context) : BaseRepository<Cinema>(context), ICinemaRepository
{
}