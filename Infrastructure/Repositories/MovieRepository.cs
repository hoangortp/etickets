﻿using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Repositories;

public class MovieRepository(AppDbContext context) : BaseRepository<Movie>(context), IMovieRepository
{
	#region Methods

	public async Task<Movie> GetAndIncludeByIdAsync(Guid id)
	{
		var movie = await DbSet.Include(mc => mc.Cinema)
							   .Include(mp => mp.Producer)
							   .Include(mam => mam.ActorsMovies).ThenInclude(ma => ma.Actor)
							   .FirstOrDefaultAsync(movie => movie.Id == id);

		return movie;
	}

	#endregion
}