﻿using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Contexts;

namespace Infrastructure.Repositories;

public class ProducerRepository(AppDbContext context) : BaseRepository<Producer>(context), IProducerRepository
{
}