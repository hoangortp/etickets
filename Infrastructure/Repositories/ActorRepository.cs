﻿using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Contexts;

namespace Infrastructure.Repositories;

public class ActorRepository(AppDbContext context) : BaseRepository<Actor>(context), IActorRepository
{
}