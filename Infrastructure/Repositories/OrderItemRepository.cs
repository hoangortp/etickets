using Domain.Aggregates.OrderAggregate;
using Domain.Entities;
using Infrastructure.Contexts;

namespace Infrastructure.Repositories;

public class OrderItemRepository(AppDbContext context) : BaseRepository<OrderItem>(context), IOrderItemRepository
{
}