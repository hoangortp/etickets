using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories;

public class ShoppingCartItemRepository(AppDbContext context) : BaseRepository<ShoppingCartItem>(context), 
                                                                IShoppingCartItemRepository
{
    public async Task<ShoppingCartItem> GetByMovieIdAndShoppingCartIdAsync(Guid movieId, string shoppingCartId)
    {
        return await Context.ShoppingCartItems.FirstOrDefaultAsync(n => n.MovieId == movieId
                                                                        && n.ShoppingCartId == shoppingCartId);
    }

    public async Task<List<ShoppingCartItem>> GetAllByShoppingCartIdIncludeMovieAsync(string shoppingCartId)
    {
        return await Context.ShoppingCartItems.Where(n => n.ShoppingCartId == shoppingCartId)
                                              .Include(n => n.Movie)
                                              .ToListAsync();
    }

    public async Task<double> GetTotalAsync(string shoppingCartId)
    {
        return await Context.ShoppingCartItems.Where(n => n.ShoppingCartId == shoppingCartId)
                                              .Select(n => n.Movie.Price * n.Amount)
                                              .SumAsync();
    }

    public async Task ClearShoppingCartAsync(string shoppingCartId)
    {
        var items = await Context.ShoppingCartItems.Where(n => n.ShoppingCartId == shoppingCartId)
                                                   .Include(n => n.Movie)
                                                   .ToListAsync();

        await RemoveRangeAsync(items);
    }
}