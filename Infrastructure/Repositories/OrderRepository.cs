using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Aggregates.OrderAggregate;
using Domain.Entities;
using Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories;

public class OrderRepository(AppDbContext context) : BaseRepository<Order>(context), IOrderRepository
{
    #region Methods

    public async Task<List<Order>> GetOrdersAsync()
    {
        return await Context.Orders.Include(o => o.User)
                                   .Include(o => o.OrderItems)
                                   .ThenInclude(o => o.Movie)
                                   .ToListAsync();
    }

    #endregion
    
}