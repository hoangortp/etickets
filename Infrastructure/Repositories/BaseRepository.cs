﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Base;
using Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories;

/// <summary>
/// Generic repository for database operations of entity of type T
/// </summary>
/// <typeparam name="T">Type of the entity in repository</typeparam>
public class BaseRepository<T> : IBaseRepository<T> where T : class, new()
{
	#nullable enable

	#region Fields

	protected readonly AppDbContext Context;
	protected readonly DbSet<T> DbSet;

	#endregion

	#region Constructors

	/// <summary>
	/// Initialize new instance of generic repository
	/// </summary>
	/// <param name="context">A DbContext to create instance of DbSet</param>
	protected BaseRepository(AppDbContext context)
	{
		Context = context;
		DbSet = Context.Set<T>();
	}

	#endregion

	#region Methods

	public async Task AddAsync(T entity) => await DbSet.AddAsync(entity);

	public async Task<ICollection<T>> GetAllAsync(Expression<Func<T, bool>>? filter = null,
												  Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
												  Expression<Func<T, object>>[]? includeProperties = null,
												  bool tracked = false)
	{
		var query = tracked ? DbSet : DbSet.AsNoTracking();

		if (filter is not null)
		{
			query = query.Where(filter);
		}

		if (orderBy is not null)
		{
			query = orderBy(query);
		}

		if (includeProperties is not null)
		{
			query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
		}

		/*if (includeProperties is not null)
		{
			foreach (Expression<Func<T, object>> includeProperty in includeProperties)
			{
				query = query.Include(includeProperty);
			}
		}*/

		return await query.ToListAsync();
	}

	public async Task<T?> GetAsync(Expression<Func<T, bool>> filter,
								   Expression<Func<T, object>>[]? includeProperties = null,
								   bool tracked = false)
	{
		var query = tracked ? DbSet : DbSet.AsNoTracking();

		query = query.Where(filter);

		if (includeProperties is not null)
		{
			query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
		}
		
		/*if (includeProperties is not null)
		{
			foreach (Expression<Func<T, object>> includeProperty in includeProperties)
			{
				query = query.Include(includeProperty);
			}
		}*/

		return await query.FirstOrDefaultAsync();
	}

	public Task RemoveAsync(T entity)
	{
		DbSet.Remove(entity);

		return Task.CompletedTask;
	}

	public Task RemoveRangeAsync(ICollection<T> entities)
	{
		DbSet.RemoveRange(entities);

		return Task.CompletedTask;
	}

	public Task UpdateAsync(T entity)
	{
		DbSet.Update(entity);

		return Task.CompletedTask;
	}

	#endregion
}